var db = require('./db')
var mongoose = require('mongoose')
mongoose.set('debug',true)
var Schema = mongoose.Schema;
var Meth;

Meth = new Schema({
	name:{type:String}
	,age:{type:Number}
	,index:{type:Number, required:false}
	,guid:{type:String, requierd:false}
	,tags:[{type:String}]
	,company:{
		name:{type:String}
		,address:{
			city: {type:String}
			,state: {type:String}
			,street: {type:String}
			,country: {type:String}
		}
	}
},{collection:'tastypie_meth'});

module.exports = db.model('Meth', Meth)
