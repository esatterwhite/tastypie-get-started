var tastypie = require('tastypie')
var MongoResource = tastypie.Resource.Mongoose;
var Meth  = require('./model')

var Resource = MongoResource.extend({
	options:{
		queryset: Meth.find().toConstructor()
	}
	,fields:{
		name: {type:'char'}
		,age:{type:'int', attribute:'age'}
		,companyName:{type:'char', attribute:'company.name', readonly:true}
	}
});

module.exports = Resource;
