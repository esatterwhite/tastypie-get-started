var hapi = require('hapi')
var tastypie = require('tastypie')
var Api = tastypie.Api
var MethResource = require('./resource')
var app
var v1;

app = new hapi.Server();

app.connection({
	port:3000,
	labels:['v1']
})

v1 = new Api('api/v1')
v1.use('meth',new MethResource() )

app.register([v1], function( e ){
	app.start( function(){
		console.log('server running at http://localhost:3000')
	} )
	
})
